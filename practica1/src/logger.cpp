#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

#define MAX_BUFFER 1024

int main(){
	int fd;
	char buffer[MAX_BUFFER];
	
	mkfifo("/tmp/mififo",0666);//los 6 dan permisos de lectura y escritura
	fd=open("/tmp/mififo", O_RDONLY);//abrir solo lectura

	if(fd<0) perror("error al abrir tuberia\n");
	
	while(1){	
	
		read(fd, buffer, sizeof(buffer));
		printf("%s\n", buffer);

		if(strcmp(buffer, "Fin del programa") ==0) break;
}	
	
	close(fd);
	unlink("/tmp/mififo");
	return 0;
}
